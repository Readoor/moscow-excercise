//
//  excercise_one_for_moscowTests.swift
//  excercise-one-for-moscowTests
//
//  Created by Evgeny Ratnikov on 27.12.17.
//  Copyright © 2017 ratnikovInco. All rights reserved.
//

import XCTest
@testable import excercise_one_for_moscow

class excercise_one_for_moscowTests: XCTestCase {
    //Инициализация необходимых переменных
    var userData:UsersData!
    var excerciseOneVC:ExcerciseOne!
    
    var firstTestDictionaryUsersEmails:[String:Set<String>] = [:]
    var secondTestDictionaryUsersEmails:[String:Set<String>] = [:]
    var thirdTestDictionaryUsersEmails:[String:Set<String>] = [:]
    
    var expectedResultFirstTestDictionary:[String:Set<String>]!
    var expectedResultSecondTestDictionary:[String:Set<String>]!
    var expectedResultThirdTestDictionary:[String:Set<String>]!
    
    override func setUp() {
        super.setUp()
        //Присваивание переменным необходимые тестовые данные
        userData = UsersData()
        excerciseOneVC = ExcerciseOne()
        
        //входные данные
        firstTestDictionaryUsersEmails = userData.inputFirstTestDictionary
        secondTestDictionaryUsersEmails = userData.inputSecondTestDictionary
        thirdTestDictionaryUsersEmails = userData.inputThirdTestDictionary
        
        //ожидаемый результат
        
        expectedResultFirstTestDictionary = ["u1": ["j", "n", "u", "d", "1", "8", "m", "3", "e", "s", "l", "g", "y", "5"]]
        
        expectedResultSecondTestDictionary = ["u9": ["w", "34", "n", "v", "u", "x", "q", "b", "8", "r", "c", "e", "7", "y", "h", "j", "o", "k", "d", "t", "1", "i", "m", "g", "l", "5"]]
        
        expectedResultThirdTestDictionary = ["u6": ["12", "34", "41", "w", "n", "v", "9", "u", "45", "22", "x", "b", "8", "r", "6", "c", "e", "7", "44", "h", "88", "y", "j", "p", "f", "o", "k", "d", "t", "33", "2", "1", "a", "i", "m", "3", "4", "s", "g", "10", "l", "0", "5"]]
    }
    
    override func tearDown() {
        userData = nil
        excerciseOneVC = nil
        super.tearDown()
    }
    
    //входные данные с 5 множествами
    func testAlgorithmWithFirstInput(){
        
        excerciseOneVC.unionUsersByEmail(emailsByUser: &firstTestDictionaryUsersEmails)
        
        XCTAssertNotNil(firstTestDictionaryUsersEmails)
        
        XCTAssertEqual(firstTestDictionaryUsersEmails, expectedResultFirstTestDictionary)
        
        XCTAssertFalse(firstTestDictionaryUsersEmails != expectedResultFirstTestDictionary)
    }
    
    //входные данные с 10 множествами
    func testAlgorithmWithSecondInput(){
        
        excerciseOneVC.unionUsersByEmail(emailsByUser: &secondTestDictionaryUsersEmails)
        
        XCTAssertNotNil(secondTestDictionaryUsersEmails)
        
        XCTAssertEqual(secondTestDictionaryUsersEmails, expectedResultSecondTestDictionary)
        
        XCTAssertFalse(secondTestDictionaryUsersEmails != expectedResultSecondTestDictionary)
    }
    
    //входные данные с 20 множествами
    func testAlgorithmWithThirdInput(){
        
        excerciseOneVC.unionUsersByEmail(emailsByUser: &thirdTestDictionaryUsersEmails)
        
        XCTAssertNotNil(thirdTestDictionaryUsersEmails)
        
        XCTAssertEqual(thirdTestDictionaryUsersEmails, expectedResultThirdTestDictionary)
        
        XCTAssertFalse(thirdTestDictionaryUsersEmails != expectedResultThirdTestDictionary)
    }
    //время выполнения алгоритма с определенными входными данными
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            
            excerciseOneVC.unionUsersByEmail(emailsByUser: &firstTestDictionaryUsersEmails)
            
//            excerciseOneVC.unionUsersByEmail(emailsByUser: &secondTestDictionaryUsersEmails)
//
//            excerciseOneVC.unionUsersByEmail(emailsByUser: &thirdTestDictionaryUsersEmails)
            
        }
    }
    
}
