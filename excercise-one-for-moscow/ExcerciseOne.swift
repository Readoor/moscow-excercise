//
//  ExcerciseOne.swift
//  excercise-one-for-moscow
//
//  Created by Evgeny Ratnikov on 27.12.17.
//  Copyright © 2017 ratnikovInco. All rights reserved.
//

import UIKit

class ExcerciseOne: UIViewController {
    
    //Алгоритм обьединения пользователей по одинаковым элементам в множестве, где A - словарь неупорядочных множеств email-aдресов с ключами именами пользователей)
    func unionUsersByEmail(emailsByUser: inout [String:Set<String>]){
        
        for nextUser in emailsByUser.keys{
            if let emailsOfFirstUser = emailsByUser[nextUser] {
                for (user, emails) in emailsByUser {
                    if !emailsOfFirstUser.isDisjoint(with: emails) {
                        if emailsOfFirstUser != emails {
                            emailsByUser[user] = emailsOfFirstUser.union(emails)
                            emailsByUser.removeValue(forKey: nextUser)
                        }
                    }
                }
            }
        }
        
    }
    
}
